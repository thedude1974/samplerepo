package domain;

import java.util.Date;

/**
 * Created by usac075 on 25-9-2015.
 */
public class Person {
    String name;
    Date birthdate;

    public Person(String name, Date birthdate) {
        this.name = name;
        this.birthdate = birthdate;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public String getName() {
        return name;
    }
}
