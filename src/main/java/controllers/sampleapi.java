package controllers;

import domain.Person;

import javax.ws.rs.Produces;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
//import org.glassfish.jersey.servlet.ServletContainer;
//import com.sun.jersey.spi.container.servlet.ServletContainer;

/**
 * Created by usac075 on 24-9-2015.
 */

@Produces (MediaType.APPLICATION_JSON)
@Consumes (MediaType.APPLICATION_JSON)
@Path ("/")
public class sampleapi {
    @Path ("/me")
    @GET
    @Produces (MediaType.APPLICATION_JSON)
    public Response getSampleResponse(
    ) {
        // TODO: write body
        List<Person> persons = new ArrayList<Person>();
        persons.add(new Person("Doede", new Date() ));
        return Response.ok().entity(persons).build();
    }

}
